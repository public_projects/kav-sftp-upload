package org.green.kav.sftp;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
@SpringBootTest
class SftpUploadApplicationTests {

	@Test
	void contextLoads() {
		LocalDateTime localDateTime = LocalDateTime.now();
		log.info("{}", localDateTime.format(DateTimeFormatter.ofPattern("yyyyMMddhmmssSSS")));
	}

}
