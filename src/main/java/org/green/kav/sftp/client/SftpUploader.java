package org.green.kav.sftp.client;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.io.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Component
public class SftpUploader implements CommandLineRunner {
    private final static Logger log = LoggerFactory.getLogger(SftpUploader.class);

    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");

    @Autowired
    private ChannelSftp channelSftp;

    @Override
    public void run(String... args) throws Exception {
        String moveDir = "D:\\opt\\ipf\\roy_dcng2_240223001_1";
        String remoteDirectory = "/opt/ipf/csv/roy_proximity_223";
        copyLocalDirToRemote(moveDir, remoteDirectory, 1000l);
    }

    public void copyLocalDirToRemote(String moveDir, String remoteDirectory, long sleepTime) throws Exception {
        StopWatch stopWatch = new StopWatch();
        LocalDateTime localDateTime = LocalDateTime.now();
        stopWatch.start("tsk-" + localDateTime.format(dateTimeFormatter));

        File fileDir = new File(moveDir);

        List<String> listFile = Arrays.asList(fileDir.list());
        Collections.sort(listFile);

        channelSftp.connect();
        FileWriter myWriter = null;
        try {
            myWriter = new FileWriter(fileDir.getAbsolutePath() + "\\" + localDateTime.toEpochSecond(ZoneOffset.UTC) + ".log");
            myWriter.write("Started at " + localDateTime.format(dateTimeFormatter) + System.lineSeparator());

            for (String fileName : listFile) {
                log.info("Moving fileName [{}] from : [{}], to: [{}]", fileName, moveDir, remoteDirectory);
                try {
                    mkdirIfNotExist(remoteDirectory);
                    copy(myWriter, new File(fileDir.getAbsolutePath() + "\\" + fileName), remoteDirectory);
                } catch (Exception ex) {
                    log.error("Failed to move file: {}, to: {}", fileName, remoteDirectory);
                    myWriter.write("Failed on directory: " + fileDir.getAbsolutePath() + "\\" + fileName);
                    myWriter.write(System.lineSeparator());
                    System.exit(1);
                }
                Thread.sleep(sleepTime);
            }

            stopWatch.stop();

            log.info("Completed in " + stopWatch.getTotalTimeSeconds() + " seconds.");
            localDateTime = LocalDateTime.now();
            myWriter.write("Completed at " + localDateTime.format(dateTimeFormatter) + System.lineSeparator());
            myWriter.write("Completed in " + stopWatch.getTotalTimeSeconds() + " seconds." + System.lineSeparator());

        }
        catch (Exception ex) {
            log.error("failed to write", ex);
        }
        finally {
            channelSftp.disconnect();
            myWriter.flush();
            myWriter.close();
        }
    }

    private void mkdirIfNotExist(String destPath) throws SftpException {
        try {
            channelSftp.cd(destPath);
        } catch (SftpException e) {
            channelSftp.mkdir(destPath);
        }
    }

    /*
     * Copies recursive
     */
    public void copy(FileWriter myWriter, File localFile, String destPath) throws IOException, SftpException {
        channelSftp.cd(destPath);
//        log.info("current remote directory: {}", channelSftp.pwd());
        myWriter.write("Moving: " + localFile.getAbsolutePath() + System.lineSeparator());
        myWriter.flush();
        if (localFile.isDirectory()) {
            channelSftp.mkdir(localFile.getName());
//            log.info("Created Folder: {} in {}", localFile.getName(), destPath);

            destPath = destPath + "/" + localFile.getName();

            channelSftp.cd(destPath);

            if (localFile.listFiles().length > 1) {
                myWriter.write("Multiple files in: " + localFile.getAbsolutePath() + System.lineSeparator());
            }

            if (localFile.listFiles().length == 0){
                myWriter.write("There was no files in: " + localFile.getAbsolutePath() + System.lineSeparator());
            }

            for (File file : localFile.listFiles()) {
                copy(myWriter, file, destPath);
            }
            myWriter.write("Done: " + localFile + System.lineSeparator());
            channelSftp.cd(destPath.substring(0, destPath.lastIndexOf('/')));
        }
        else {
            channelSftp.put(new FileInputStream(localFile), localFile.getName(), ChannelSftp.OVERWRITE);
        }
        myWriter.flush();
    }

    public void findFolders(String parentPath) throws SftpException, FileNotFoundException {
        Vector v = channelSftp.ls(parentPath);

        Optional<String> latestPath = v.stream()
                                       .map(o -> getDirectory(o))
                                       .filter(o -> !((String) o).isEmpty())
                                       .filter(o -> isValidDirectory((String) o))
                                       .sorted(Comparator.reverseOrder())
                                       .findFirst();

        if (!latestPath.isPresent()) {

        }

        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter.ofPattern("yyyyMMddhmssSSS");
    }


    private String getDirectory(Object o) {
        log.info("V {}", o.toString());
        String lsDetails = o.toString();
        return lsDetails.substring(lsDetails.lastIndexOf(" "), lsDetails.length())
                        .trim();
    }

    private boolean isValidDirectory(String lsDetails) {
        if (!lsDetails.equalsIgnoreCase(".") && !lsDetails.equalsIgnoreCase("..")) {

            if (lsDetails.contains("export")) {
                return true;
            }
        }

        return false;
    }
}
