package org.green.kav.sftp.config;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "remote-host")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class SftpAppConfig {

    private String hostIp;
    private String username;
    private String password;

    @Bean
    public ChannelSftp setupJsch() throws JSchException {
        JSch jsch = new JSch();
        jsch.setKnownHosts("C:\\Users\\sm13\\.ssh\\known_hosts");
        Session jschSession = jsch.getSession(username, hostIp);
        jschSession.setPassword(password);
        jschSession.connect();
        return (ChannelSftp) jschSession.openChannel("sftp");
    }

    @Bean
    protected Session getSession(@Value("${remote-host.username}") String username,
                                 @Value("${remote-host.password}") String password,
                                 @Value("${remote-host.hostIp}") String host) throws Exception {
        Session session = new JSch().getSession(username, host);
        session.setPassword(password);
        session.setConfig("StrictHostKeyChecking", "no");
        session.connect();
        return session;
    }
}
